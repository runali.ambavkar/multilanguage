package com.example.demoapp

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    var strlocale:String = ""
//    var editor :Nothing ?= null;
//    init {
//         editor = getSharedPreferences("Settings",Context.MODE_PRIVATE).edit().commit() as Nothing
//
//    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        loadLoacte()
        setContentView(R.layout.activity_main)


        val radioButton:RadioButton=findViewById(R.id.rdEng)
        val radioButton1:RadioButton=findViewById(R.id.rdArb)
       // editor= getSharedPreferences("Settings",Context.MODE_PRIVATE).edit() as Nothing?
        btn_newCase.setOnClickListener(View.OnClickListener {
        Toast.makeText(this,strlocale,Toast.LENGTH_LONG).show()
        val intent =  Intent(this, NewCase::class.java)

            startActivity(intent)
//            finish()
        })

        val listitem= arrayListOf<String>()
        listitem.add("ar")
        listitem.add("en")

        radioGrp.setOnCheckedChangeListener { group, checkedId ->
            if(radioButton.isChecked)
            {
                strlocale="en"
                setLocaleinApp(strlocale)
            }
            else if(radioButton1.isChecked)
            {
                strlocale="ar"
                setLocaleinApp(strlocale)
            }
        }
    }

    fun setLocaleinApp( lang:String)
    {
        val locale=Locale(lang)
        Locale.setDefault(locale)
        val config=Configuration()
        config.locale=locale
        baseContext.resources.updateConfiguration(config,baseContext.resources.displayMetrics)
        updateText()
//        val editor=getSharedPreferences("Settings",Context.MODE_PRIVATE).edit()
//        editor.putString("myLang",lang)
//        editor.apply()
    }

    fun updateText()
    {
//      setLocaleinApp(language)
        btn_newCase.setText(this.getString(R.string.new_cases))
        btnViewAll.setText(R.string.view_all)
    }
}


