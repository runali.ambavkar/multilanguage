package com.example.demoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.get
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.badoualy.stepperindicator.StepperIndicator
import com.example.demoapp.new_cases.adapters.viewPagerAdapter


class NewCase : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_case)





        val viewpager=findViewById<ViewPager>(R.id.pager)

        val adapter = viewPagerAdapter(supportFragmentManager)
        viewpager.adapter = adapter

        val stepper=findViewById<StepperIndicator>(R.id.stepper_indicator)

//        stepper.setViewPager(viewpager,4)

        stepper.addOnStepClickListener( StepperIndicator.OnStepClickListener {

            StepperIndicator.OnStepClickListener {

                 viewpager.setCurrentItem(stepper.currentStep,true) }
         })
    }
}
