package com.example.demoapp.new_cases.view


import android.Manifest.permission
import android.content.ContextWrapper
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat

import com.example.demoapp.R
import kotlinx.android.synthetic.main.fragment_stage_three.*
import kotlinx.android.synthetic.main.fragment_stage_three.view.*
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.jar.Manifest


class stage_three : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?
    {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_stage_three, container, false)

        view.btnsave.setOnClickListener{ view->

            val dir= Environment.getExternalStorageDirectory().toString()

            val file=File(dir,"images")
            val stream :OutputStream=FileOutputStream(file)

            val bitmap= signature_view.signatureBitmap
            bitmap.compress(Bitmap.CompressFormat.PNG,100,stream)
        }

        view.btnClear.setOnClickListener{
            signature_view.clearCanvas()
        }

   return view;
    }


}
