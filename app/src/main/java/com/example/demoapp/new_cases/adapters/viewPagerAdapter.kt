package com.example.demoapp.new_cases.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.demoapp.new_cases.view.stage_one
import com.example.demoapp.new_cases.view.stage_three
import com.example.demoapp.new_cases.view.stage_two

class viewPagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm!!) {

    override fun getItem(position: Int): Fragment {

        var fragment:Fragment?=null
        when (position)
        {
           0->fragment=stage_one()
            1->fragment=stage_two()
            2->fragment=stage_three()
            3->fragment=stage_three()

        }
        return fragment!!
    }

    override fun getCount(): Int {
        return 4
    }

    override fun getPageTitle(position: Int): CharSequence? {
        when (position)
        {
            1->"Stage 1"
            2->"Stage 2"
            4->"Stage 4"
            3->"Stage 3"
        }

        return super.getPageTitle( position)
    }

}
